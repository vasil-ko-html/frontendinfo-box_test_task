var helpers = (function () {
  var browsers = ['MSIE', 'Firefox', 'Safari', 'Chrome', 'Opera'],
      userAgent = navigator.userAgent,
      $html = $('html');

  function ieCheck() {
    var re  = new RegExp("MSIE ([0-9]{1,}[\.0-9]{0,})");
    var rv = -1; // Return value assumes failure.
    if (navigator.appName == 'Microsoft Internet Explorer')
    {
      if (re.exec(userAgent) != null) {
        rv = parseFloat(RegExp.$1);
      }
    }
    return rv;
  }

  function addClass(ver) {
    ver = ver || 0;
    if(ver > 0) {
      $html.addClass('ie' + ver);
    }
    else {
      return false;
    }
  }

  function readJSON(filename) {
    filename = filename;
    /*$.getJSON(filename, function(data) {
      return data;
    });*/
    $.ajax({
      url: filename + '?callback=?',
      dataType: 'jsonp'
    });
  }

  return {
    browser: {
      ie: ieCheck,
      addClass: addClass
    },
    json: {
      read: readJSON
    }
  }
})();