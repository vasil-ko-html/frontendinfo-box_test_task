var helpers = (function () {
  var browsers = ['MSIE', 'Firefox', 'Safari', 'Chrome', 'Opera'],
      userAgent = navigator.userAgent,
      $html = $('html');

  function ieCheck() {
    var re  = new RegExp("MSIE ([0-9]{1,}[\.0-9]{0,})");
    var rv = -1; // Return value assumes failure.
    if (navigator.appName == 'Microsoft Internet Explorer')
    {
      if (re.exec(userAgent) != null) {
        rv = parseFloat(RegExp.$1);
      }
    }
    return rv;
  }

  function addClass(ver) {
    ver = ver || 0;
    if(ver > 0) {
      $html.addClass('ie' + ver);
    }
    else {
      return false;
    }
  }

  function readJSON(filename) {
    filename = filename;
    /*$.getJSON(filename, function(data) {
      return data;
    });*/
    $.ajax({
      url: filename + '?callback=?',
      dataType: 'jsonp'
    });
  }

  return {
    browser: {
      ie: ieCheck,
      addClass: addClass
    },
    json: {
      read: readJSON
    }
  }
})();
var itemsCarousel = function(options) {

  var width = 312,
      height = 312,
      id = 0,
      $holder = $('.js-items-carousel'),
      $items = $('.js-item-carousel', $holder),
      itemsLength = $items.length,
      $toStore = $('.js-to-store', $holder),
      $description = $('.js-item-short-describe', $items),
      $toggleLink = $('.js-toggle-link', $items),
      $buttons = $('.js-btn-prev, .js-btn-next', $holder);

  function increment() {
    return ++id;
  }

  function decrement() {
    return --id;
  }

  function resetId(n) {
    n = n || 0;
    if(n > itemsLength || n < 0) {
      n = 0;
    }
    return id = n;
  }

  function getDirectionCarousel(classes) {
    classes = classes || [];
    classesLength = classes.length;
    var obj = {},
        dir;
    for(var i = 0; i < classesLength; i++) {
      obj[classes[i]] = classes[i];
    }
    if(obj['left'] in obj || obj['right'] in obj) {
      dir = obj['left'] || obj['right'];
    }
    return dir;
  }

  function generateURLToStore(itemId) {
    
    itemId = itemId || 1;
    if(itemId < 1) {
      itemId = 4;
    } else if(itemId > 4) {
      itemId = 1
    }
    var param = {
      relPath: '/products',
      name: '/promo',
      id: itemId,
      ext: '.html'
    },
        link;

    return link = param.relPath + param.name + param.id + param.ext;
  }

  function clickBtn() {
    $buttons.on('click', function() {
      var $currentBtn = $(this),
          classes = $currentBtn.attr('class').split(' '),
          dir = getDirectionCarousel(classes);

      //console.log(itemsLength, options);

      if(dir == 'right') {
        increment();
        $toStore.attr('href', generateURLToStore(id+1));
        if(id >= itemsLength) {
          resetId();
        }
        $($items[id]).fadeIn().siblings().fadeOut();
      }
      else {
        decrement();
        $toStore.attr('href', generateURLToStore(id));
        if(id < 0) {
          resetId(itemsLength - 1);
        }
        $($items[id]).fadeIn().siblings().fadeOut();
      }

      console.log($items[id], id);
    });
  }

  function toggleDescription() {
    $toggleLink.on('click', function(e) {
      e.preventDefault();
      var $currentLink = $(this),
          $parent = $(this).parent(),
          $currentHolder = $('.js-image-carousel', $parent.parent());

      $currentHolder.animate({
        opacity: 'toggle',
        marginTop: 'toggle'
      }, 500, function() {
        $parent.toggleClass('active');
        $currentLink.text() == 'hide details' ? $currentLink.text('show details') : $currentLink.text('hide details');
      });
    });
  }

  function initCarousel() {
    resetId();
    clickBtn();
    toggleDescription();
    $toStore.attr('href', generateURLToStore(id+1));
  }

  return {
    init: initCarousel
  }
};