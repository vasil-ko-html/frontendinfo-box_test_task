'use strict';
module.exports = function(grunt) {
  grunt.config('less', {
    development: {
      files: {
        '../project/styles/s.css': '../project/styles/s.less'
      }
    },
    prod: {
      options: {
        compress: true,
        modifyVars: grunt.file.readJSON('../project/components/carousel/data/less_config.json')
      },
      files: {
        '../project/styles/s.css': '../project/styles/s.less'
      }
    }
  });

  grunt.loadNpmTasks('grunt-contrib-less');
};