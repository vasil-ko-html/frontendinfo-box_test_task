'use strict';
module.exports = function(grunt) {
  grunt.config('jade', {
    options: {
      pretty: true,
      compileDebug: false,
      data: {
        config: grunt.file.readJSON('../project/components/carousel/data/info_box.json'),
        t_config: grunt.file.readJSON('../project/components/carousel/data/template_config.json')
      }
    },
    dev: {
      files: {
        '../project/html/index.html': ['../project/components/carousel/template/*.jade']
      }
    },
    prod: {
      options: {
        client: true
      },
      files: {
        '../project/components/carousel/template/template.jade.js': ['../project/components/carousel/template/*.jade']
      }
    }
  });

  grunt.loadNpmTasks('grunt-contrib-jade');
};