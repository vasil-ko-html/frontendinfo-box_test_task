'use strict';
module.exports = function(grunt) {
  grunt.config('concat', {
    dev: {
      src: [
        '../project/js/helpers.js',
        '../project/js/itemsCarousel.js'
      ],
      dest: '../project/js/main.js',
    }
  });

  grunt.loadNpmTasks('grunt-contrib-concat');
};