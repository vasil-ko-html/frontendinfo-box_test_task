'use strict';
module.exports = function(grunt) {
  grunt.config('sprite', {
    development: {
      'cssTemplate': 'spritesmith-template.mustache',
      'padding': 2,
      'imgPath': '../images/sprite.png',
      'cssFormat': 'less',
      'algorithm': 'binary-tree',
      'src': ['../project/images/sprite-img/*.png'],
      'dest': '../project/images/sprite.png',
      'destCss': '../project/styles/sprite.less'
    }
  });

  grunt.loadNpmTasks('grunt-spritesmith');
};