'use strict';
module.exports = function(grunt) {
  grunt.config('uglify', {
    dev: {
      files: {
        '../project/js/main.min.js': ['../project/js/main.js'],
        '../project/js/jquery.min.js': ['../project/js/jquery.js']
      }
    }
  });

  grunt.loadNpmTasks('grunt-contrib-uglify');
};