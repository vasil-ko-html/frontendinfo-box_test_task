'use strict';
module.exports = function(grunt) {
  grunt.config('csscomb', {
    options: {
        config: 'csscomb-config.json'
      },
      dev: {
        files: {
          '../project/styles/s.css': ['../project/styles/s.css']
        }
      }
  });

  grunt.loadNpmTasks('grunt-csscomb');
};