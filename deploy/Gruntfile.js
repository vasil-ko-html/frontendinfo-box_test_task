module.exports = function(grunt) {

  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json')
  });

  //Load task(s)
  grunt.loadTasks('./tasks');

  // Task(s).
  // Deploy sprite, sorted via CSSComb, Less -> CSS, Jade -> HTML
  grunt.registerTask('default', ['sprite:development', 'csscomb:dev', 'less:development', 'jade:dev']);

  //Jade -> HTML
  grunt.registerTask('jadeToHtml', ['jade:dev']);

  //Run when you're ready to productions
  grunt.registerTask('production', ['sprite:development', 'less:prod', 'csscomb:dev', 'jade:prod', 'concat:dev', 'uglify:dev']);
};