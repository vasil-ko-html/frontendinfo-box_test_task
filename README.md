# FrontEndinfo-box_test_task #

### Usage ###
* Download `https://bitbucket.org/vasil-ko-html/frontendinfo-box_test_task/get/d2270efe6460.zip`
* Unzip
* cd to ./deploy
* run npm install
* cd ./
* run bower install
* cd to ./deploy
* run grunt for dev
* run grunt jadeToHTML for dev
* run grunt production for production

### Development ###
`git clone bitbucket.org/vasil-ko-html/frontendinfo-box_test_task.git`

### Dependencies ###
* [npm](https://www.npmjs.com)
* [Node](https://nodejs.org/)
* [Grunt](http://gruntjs.com/)
* Grunt plugins (npm list)
* [Jade](http://jade-lang.com/)
* [Less](http://lesscss.org/)